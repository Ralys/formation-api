const colors = require('colors');
const fs = require('fs-extra');
const swaggerJSDoc = require('swagger-jsdoc');

const config = require('../app/config');

(async () => {
    const options = {
        swaggerDefinition: {
            info: {
                title: `${config.app_name}`,
                version: `${config.version}`
            },
        },
        apis: ['./app/routes/*.js', './app/models/schemas/*.js', './app/models/dto/*.js']
    };

    await fs.writeJson('swagger.json', swaggerJSDoc(options));
    console.info(`Swagger file generated`.green);
})();
