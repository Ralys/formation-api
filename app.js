const colors = require('colors');
const express = require('express');
const bodyParser = require('body-parser');
const errorHandler = require('./app/utils/error.handler');

const config = require('./app/config');
const registerRoutes = require('./app/routes');

const app = express();
const router = express.Router();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(`/${config.root_url}`, registerRoutes(router, config));
app.use(errorHandler);

app.listen(config.port, () => {
    console.info(`The service ${config.app_name} started on http://localhost:${config.port}`.blue);
});

module.exports = app;