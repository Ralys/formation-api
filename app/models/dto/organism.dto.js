/**
 * @swagger
 * definitions:
 *   Organism:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *         format: int64
 *       name:
 *         type: string
 *       description:
 *         $ref: '#/definitions/Translation'
 *       createdAt:
 *         type: string
 *         example: 1977-04-22T06:00:00Z
 *       updatedAt:
 *         type: string
 *         example: 1977-04-22T06:00:00Z
 *        
 */
class OrganismDto {

    constructor(organism) {
        this.id = organism._id
        this.name = organism.name;
        this.description = organism.description;
    }

}

module.exports = OrganismDto;