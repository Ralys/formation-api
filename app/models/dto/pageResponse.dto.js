class PageResponse {

    constructor(first, total, data) {
        this.first = first;
        this.total = total;
        this.data = data;
    }

}

module.exports = PageResponse;