const _ = require('lodash');

class SessionLightDto {
    
    constructor(session) {
        this.id = session._id;
        this.year = session.year;
        this.start = session.start;
        this.end = session.end;
        this.location = session.location;

        /** Computed **/
        this.numberOfParticipants = session.participations.length;
        this.numberOfCertified = session.participations.filter(
            participation =>  !_.isUndefined(participation.certification)
        ).length;
    }

}

class SessionDto {
    
    constructor(session) {
        this.id = session._id;
        this.year = session.year;
        this.start = session.start;
        this.end = session.end;
        this.location = session.location;
        this.participations = session.participations;
    }

}

module.exports = { SessionLightDto, SessionDto };