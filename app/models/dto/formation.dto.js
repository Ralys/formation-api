const _ = require('lodash');
const moment = require('moment');
const { SessionLightDto } = require('./session.dto');

class FormationLightDto {
    
    constructor(formation, organismDto) {
        this.id = formation._id;
        this.name = formation.name;
        this.organism = organismDto;
        this.duration = formation.duration;
        this.certifiable = formation.certifiable;
        
        /** Computed **/
        this.averageNote = _.sum(formation.sessions.map(session => 
            _.sum(session.participations.map(participation => participation.note)) /  session.participations.length
        )) / formation.sessions.length;

        this.numberCertified = formation.certifiable ? 
        _.sum(formation.sessions.map(session => 
            session.participations
                .filter(participation => !_.isUndefined(participation.certification)).length
        )) : undefined;

        this.isSessionCurrentlyInProgress = formation.sessions.filter(session =>
            moment().isBetween(session.start, session.end)
        ).length > 0;
    }

}

class FormationDto extends FormationLightDto {

    constructor(formation, organismDto) {
        super(formation, organismDto);
        this.sessions = formation.sessions.map((session) => new SessionLightDto(session));
    }

}

module.exports = { FormationLightDto, FormationDto };