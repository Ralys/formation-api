const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const organismSchema = new Schema({
    name:  String,
    description: {
        fr: String,
        en: String,
        de: String
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Organism', organismSchema);