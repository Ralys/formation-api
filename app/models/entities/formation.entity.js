const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const formationSchema = new Schema({
    name:  String,
    description: {
        fr: String,
        en: String,
        de: String
    },
    organism: String,
    duration: Number,
    certifiable: Boolean,
    sessions: [
        {
            _id: String,
            year: Number,
            start: { type: Date, default: Date.now },
            end: { type: Date, default: Date.now },
            location: String,
            participations: [
                {
                    _id: String,
                    participant: String,
                    note: Number,
                    comment: String,
                    certification: {
                        default: undefined,
                        type: {
                            start: { type: Date, default: Date.now },
                            end: { type: Date, default: Date.now },
                            duration: Number,
                            image: String
                        }
                    }
                }
            ]
        }
    ],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Formation', formationSchema);