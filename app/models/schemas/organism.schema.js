const Joi = require('joi');
const Translation = require('./translation.schema');
const PageRequest = require('./pageRequest.schema');

/**
 * @swagger
 * parameters:
 *   Organism_name:
 *     in: query
 *     description: name of the organism
 *     name: name
 *     type: string
 *        
 */
const GetOrganismsRequest = PageRequest.keys({
    name: Joi.string()
});

/**
 * @swagger
 * definitions:
 *   InputOrganismRequest:
 *     type: object
 *     required: 
 *       - name
 *     properties:
 *       name:
 *         type: string
 *       description:
 *         $ref: '#/definitions/Translation'
 *        
 */
const InputOrganismRequest = Joi.object().keys({
    name: Joi.string().required(),
    description: Translation
});

module.exports = { GetOrganismsRequest, InputOrganismRequest };