const Joi = require('joi');

/**
 * @swagger
 * definitions:
 *   Translation:
 *     type: object
 *     required:
 *       - fr
 *     properties:
 *       fr:
 *         type: string
 *       en:
 *         type: string
 *       de:
 *         type: string
 *
 */
const Translation = Joi.object().keys({
    fr: Joi.string().required(),
    en: Joi.string(),
    de: Joi.string()
});

module.exports = Translation;