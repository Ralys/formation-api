const Joi = require('joi');
const PageRequest = require('./pageRequest.schema');

/**
 * @swagger
 * parameters:
 *   Formation_name:
 *     in: query
 *     description: name of the formation
 *     name: name
 *     type: string
 * 
 *   Formation_certifiable:
 *     in: query
 *     description: the formation comes with a certification
 *     name: certifiable
 *     type: boolean
 *
 *   Formation_isSessionCurrentlyInProgress:
 *     in: query
 *     description: indicator if session is currently in progress
 *     name: isSessionCurrentlyInProgress
 *     type: boolean
 *         
 */
const GetFormationsRequest = PageRequest.keys({
    name: Joi.string(),
    certifiable: Joi.boolean()
    // isSessionCurrentlyInProgress: Joi.boolean()
});

module.exports = { GetFormationsRequest };