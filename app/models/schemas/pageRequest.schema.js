const Joi = require('joi');

/**
 * @swagger
 * parameters:
 *   first:
 *     in: query
 *     name: first
 *     description: number of elements to skip
 *     type: integer
 *     format: int64
 *   limit:
 *     in: query
 *     name: limit
 *     description: maximum number of elements to retrieve
 *     type: integer
 *     format: int64
 *        
 */
const PageRequest = Joi.object().keys({
    first: Joi.number().min(0).default(0),
    limit: Joi.number().max(50).default(10)
});

module.exports = PageRequest;