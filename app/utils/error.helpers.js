const HttpStatus = {
    BadRequest: { label: 'BadRequest', code: 400 },
    Unauthorized: { label: 'Unauthorized', code: 401 },
    Forbidden: { label: 'Forbidden', code: 403 },
    NotFound: { label: 'NotFound', code: 404 },
    InternalServerError: { label: "InternalServerError", code: 500 }
};

const computeErrorCode = (error) => {
    const match = Object.values(HttpStatus).find(e => e.label == error.name);
    return match ? match.code : HttpStatus.InternalServerError.code;
}

class BadRequest extends Error {
    get name() {
        return HttpStatus.BadRequest.label;
    }
}

class Unauthorized extends Error {
    get name() {
        return HttpStatus.Unauthorized.label;
    }
}

class Forbidden extends Error {
    get name() {
        return HttpStatus.Forbidden.label;
    }
}

class NotFound extends Error {
    get name() {
        return HttpStatus.NotFound.label
    }
}

class InternalServerError extends Error {
    get name() {
        return HttpStatus.InternalServerError.label
    }
}

module.exports = { computeErrorCode, BadRequest, Unauthorized, Forbidden, NotFound, InternalServerError }