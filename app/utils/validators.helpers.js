const Joi = require('joi');

const { isObjectId, sanitize } = require('../utils/string.helpers');
const { BadRequest } = require('../utils/error.helpers');

const validateSchema = (object, schema) => {
    const joiResult = Joi.validate(sanitize(object), schema);
    
    if(joiResult.error) {
        throw new BadRequest(joiResult.error);
    }

    return joiResult.value;
};

const validateObjectId = (element, name) => {
    if(!isObjectId(element)) {
        throw new BadRequest(`The ${name} must be an ObjectId`);
    }

    return element;
}

module.exports = { validateSchema, validateObjectId };