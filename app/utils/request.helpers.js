class RequestBuilder {

    constructor(request) {
        this.request = request;
        this.likes = [];
    }

    setLikeFor(...properties) {
        this.likes = this.likes.concat(properties);
        return this;
    }

    build() {
        const formattedRequest = { ...this.request };

        this.likes.forEach(like => {
            if(formattedRequest[like]) {
                formattedRequest[like] = { $regex: this.request[like], $options: 'i' };
            }
        });

        return formattedRequest;
    }

}

module.exports = { RequestBuilder };