const ObjectId = require('mongoose').Types.ObjectId;

const decodeParameter = (str) => decodeURIComponent((str+'').replace(/\+/g, '%20'));

module.exports = {

    isObjectId: (obj) => ObjectId.isValid(obj),

    sanitize: (obj) => {
        const sanitizedObj = {};

        for(let key in obj) {
            const value = obj[key];

            if(typeof(value) === 'string') {
                sanitizedObj[key] = decodeParameter(value);
            } else {
                sanitizedObj[key] = value;
            }
        }

        return sanitizedObj;
    }

};