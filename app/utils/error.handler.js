const colors = require('colors');
const moment = require('moment');
const { computeErrorCode } = require('./error.helpers');

const now = () => moment().format('DD/MM/YYYY HH:mm:ss');

module.exports = (err, req, res, next) => {
    console.error(`${now()} ${err.message} ${err.stack}`.red);
    
    const response = { error: err.message };
    res.status(computeErrorCode(err)).json(response);
};