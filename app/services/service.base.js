const moment = require('moment');
const now = () => moment().format('DD/MM/YYYY HH:mm:ss');

class Service {
    
    log(tag, ...args) {
        console.log(`${now()} ${this.constructor.name}#${tag}`, ...args);
    }

}

module.exports = Service;