const Service = require('./service.base.js');
const PageResponse = require('../models/dto/pageResponse.dto');
const { FormationLightDto, FormationDto } = require('../models/dto/formation.dto');
const { SessionDto } = require('../models/dto/session.dto');
const { RequestBuilder } = require('../utils/request.helpers');
const { NotFound } = require('../utils/error.helpers');

const formationNotFound = (id) => new NotFound(`Formation with id "${id}" does not exist`);

module.exports = ({ Formation, organismService }) => {

    class FormationService extends Service {

        async findAll(first, limit, request) {
            this.log("findAll", { first, limit, request });

            const formattedRequest = new RequestBuilder(request).setLikeFor('name').build();
    
            const total = await Formation.count(formattedRequest);
            const formations = await Formation.find(formattedRequest).skip(first).limit(limit);

            const organisms = formations.length == 0 ? [] : await organismService.findByMultipleIds(
                formations.map(formation => formation.organism)
            );

            const content = formations.map(
                (formation, i) => new FormationLightDto(
                    formation, 
                    organisms.find(organism => organism.id == formation.organism)
                )
            );

            return new PageResponse(first, total, content);
        }
        
        async findById(id) {
            this.log("findById", `id = ${id}`);

            const formation = await Formation.findOne({ _id: id });

            if(!formation) {
                throw formationNotFound(id);
            }

            const organism = await organismService.findById(formation.organism);

            return new FormationDto(formation, organism);
        }

        async findSessionByIds(idFormation, idSession) {
            this.log("findSessionByIds", `idFormation = ${idFormation} idSession = ${idSession}`);

            const formation = await Formation.findOne({ _id: idFormation });

            if(!formation) {
                throw formationNotFound(id);
            }

            const session = formation.sessions.find(session => session._id === idSession);

            if(!session) {
                throw new NotFound(
                    `Session with formation id "${idFormation}" and session id "${idSession}" does not exist`
                );
            }

            return new SessionDto(session);
        }

    }

    return new FormationService();
} 