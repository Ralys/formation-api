const Service = require('./service.base.js');
const OrganismDto = require('../models/dto/organism.dto');
const PageResponse = require('../models/dto/pageResponse.dto');
const { RequestBuilder } = require('../utils/request.helpers');
const { NotFound } = require('../utils/error.helpers');

const toDto = (organismFromMongo) => new OrganismDto(organismFromMongo);
const organismNotFound = (id) => new NotFound(`Organism with id "${id}" does not exist`);

module.exports = ({ Organism }) => {

    class OrganismService extends Service {
        
        async findAll(first, limit, request) {
            this.log("findAll", { first, limit, request });

            const formattedRequest = new RequestBuilder(request).setLikeFor('name').build();
    
            const total = await Organism.count(formattedRequest);
            const organisms = await Organism.find(formattedRequest).skip(first).limit(limit);
            const content = organisms.map(toDto);
    
            return new PageResponse(first, total, content);
        }
    
        async findById(id) {
            this.log("findById", `id = ${id}`);
    
            const organism = await Organism.findOne({ _id: id });
            
            if(!organism) {
                throw organismNotFound(id);
            }
    
            return toDto(organism);
        }

        async findByMultipleIds(ids) {
            this.log("findByMultipleIds", `ids = ${ids}`);

            const organisms = await Organism.find({ _id : { $in: ids } });

            return organisms.map(toDto);
        }
    
        async create(request) {
            this.log("create", request);
    
            const organism = await Organism.create(request);
            return toDto(organism);
        }
    
        async updateBydId(id, request) {
            this.log("updateBydId", `id = ${id}`, request);
            
            const organism = await Organism.findOne({ _id: id });
            
            if(!organism) {
                throw organismNotFound(id);
            }
    
            organism.set(request);
            const updatedOrganism = await organism.save();
    
            return toDto(updatedOrganism);
        }
    
        async deleteById(id) {
            this.log("deleteById", `id = ${id}`);
    
            const organism = await Organism.findOne({ _id: id });
            
            if(!organism) {
                throw organismNotFound(id);
            }
    
            await Organism.remove(organism);
        }
    
    }

    return new OrganismService();
} 