const Organism = require('../models/entities/organism.entity');
const Formation = require('../models/entities/formation.entity');

const organismService = require('./organism.service')({ Organism });
const formationService = require('./formation.service')({ Formation, organismService });

module.exports = { organismService, formationService };