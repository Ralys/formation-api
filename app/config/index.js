const environment = process.env.APP_ENV || 'Development';
const config = require(`./config.${environment}`);

module.exports = config;