const wrap = require('../utils/wrapper.helper');

const { validateObjectId, validateSchema } = require('../utils/validators.helpers');
const { GetFormationsRequest } = require('../models/schemas/formation.schema');

const context = 'formations';

module.exports = (router, { formationService }) => {

    router.get(`/${context}`, wrap(async (req, res) => {
        const validatedRequest = validateSchema(req.query, GetFormationsRequest);

        const { first, limit, ...request } = validatedRequest;
        const formations = await formationService.findAll(first, limit, request);
        res.json(formations);
    }));

    router.get(`/${context}/:id`, wrap(async(req, res) => {
        const id = validateObjectId(req.params.id, "id of the formation");

        const formation = await formationService.findById(id);
        res.json(formation);
    }));

    router.get(`/${context}/:idFormation/sessions/:idSession`, wrap(async(req, res) => {        
        const idFormation = validateObjectId(req.params.idFormation, "id of the formation");
        const idSession = validateObjectId(req.params.idSession, "id of the session");

        const session = await formationService.findSessionByIds(idFormation, idSession);
        res.json(session);
    }));

};