const mongoose = require('mongoose');
const services = require('../services');
const organisms = require('./organisms.route');
const formations = require('./formations.route');

module.exports = (router, config) => {
    mongoose.Promise = global.Promise
    mongoose.connect(
        `mongodb://${config.db.host}:${config.db.port}/${config.db.database}`, 
        { useMongoClient: true }
    );

    organisms(router, services);
    formations(router, services);

    return router;
};