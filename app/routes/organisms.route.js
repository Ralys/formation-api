const wrap = require('../utils/wrapper.helper');

const { validateObjectId, validateSchema } = require('../utils/validators.helpers');
const { GetOrganismsRequest, InputOrganismRequest } = require('../models/schemas/organism.schema');

const context = 'organisms';

module.exports = (router, { organismService }) => {

    router.get(`/${context}`, wrap(async (req, res) => {
        const validatedRequest = validateSchema(req.query, GetOrganismsRequest);

        const { first, limit, ...request } = validatedRequest;
        const organisms = await organismService.findAll(first, limit, request);
        res.json(organisms);
    }));

    router.get(`/${context}/:id`, wrap(async(req, res) => {
        const id = validateObjectId(req.params.id, "id of the organism");

        const organism = await organismService.findById(id);
        res.json(organism);
    }));

    router.post(`/${context}`, wrap(async (req, res) => {
        const validatedRequest = validateSchema(req.body, InputOrganismRequest);

        const organism = await organismService.create(validatedRequest);
        res.json(organism);
    }));

    router.put(`/${context}/:id`, wrap(async(req, res) => {
        const id = validateObjectId(req.params.id, "id of the organism");
        const validatedRequest = validateSchema(req.body, InputOrganismRequest);

        const organism = await organismService.updateBydId(id, validatedRequest);
        res.json(organism);
    }));

    router.delete(`/${context}/:id`, wrap(async(req, res) => {
        const id = validateObjectId(req.params.id, "id of the organism");

        const organism = await organismService.deleteById(id);
        res.json();
    }));

};