const chai = require('chai');
const { expect } = chai;

const { isObjectId, sanitize } = require('../../app/utils/string.helpers');

describe("String Helpers", () => {
    
    describe("isObjectId", () => {
        it("should validate a string as an object id", () => {
            const result = isObjectId('59ea3603f3cebdd601497017');
            expect(result).to.be.true;
        })
    })

    describe("sanitize", () => {
        it("should replace all the + by spaces from string", () => {
            const {element} = sanitize({ element: "Test+data" });
            expect(element).to.be.a('string').that.does.not.contain('+');
            expect(element).to.be.a('string').that.contains(' ');
        })

        it("should replace all the %20 by spaces from string", () => {
            const {element} = sanitize({ element: "Test%20data" });
            expect(element).to.be.a('string').that.does.not.contain('%20');
            expect(element).to.be.a('string').that.contains(' ');
        })
    })

})