const chai = require('chai');
const { expect } = chai;

const { RequestBuilder } = require('../../app/utils/request.helpers');

describe("Request Helpers", () => {
    
    describe("RequestBuilder", () => {
        it("should build a request with name as a query object and type as a string", () => {
            const request = {
                name: "red",
                type: "square"
            };

            const formattedRequest = new RequestBuilder(request).setLikeFor('name').build();
            
            expect(formattedRequest).to.have.all.keys('name', 'type');
            expect(formattedRequest.name).to.be.an('object');
            expect(formattedRequest.name).to.have.all.keys('$regex', '$options');
            expect(formattedRequest.name.$regex).to.be.equals("red");
            expect(formattedRequest.name.$options).to.be.equals('i');
            expect(formattedRequest.type).to.be.a('string');
            expect(formattedRequest.type).to.be.equals("square");
        })
    })

})