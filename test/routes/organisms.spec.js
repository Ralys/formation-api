const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = chai;

const config = require('../../app/config');
const api = require('../../app');

chai.use(chaiHttp);

describe("[INTEGRATION] Organism routes", () => {

    describe("GET /organisms", () => {
        it("should list all organisms", (done) => {
            chai.request(api)
                .get(`/${config.root_url}/organisms`)
                .end((err, res) => {
                    const { body } = res;

                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    expect(body).to.have.all.keys('first', 'total', 'data');
                    expect(body.data).to.be.an('array');
                    
                    if(body.data.length > 0) {
                        expect(body.data[0]).to.have.all.keys('id', 'name', 'description');
                    }

                    done();
                })
        })
    }) 

})
