const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = chai;

const config = require('../../app/config');
const api = require('../../app');

chai.use(chaiHttp);

describe("[INTEGRATION] Formations routes", () => {

    describe("GET /formations", () => {
        it("should list all organisms", (done) => {
            chai.request(api)
                .get(`/${config.root_url}/formations`)
                .end((err, res) => {
                    const { body } = res;

                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    expect(body).to.have.all.keys('first', 'total', 'data');
                    expect(body.data).to.be.an('array');
                    
                    if(body.data.length > 0) {
                        const formation = body.data[0];

                        expect(formation).to.be.an('object').that.includes.all.keys(
                            'id', 'name', 'organism', 'duration', 'certifiable',
                            'averageNote', 'isSessionCurrentlyInProgress'
                        );
                        
                        expect(formation.name).to.be.a('string');
                        expect(formation.duration).to.be.a('number');
                        expect(formation.averageNote).to.be.a('number');
                        expect(formation.isSessionCurrentlyInProgress).to.be.a('boolean');
                        expect(formation.organism).to.be.an('object').that.has.all
                            .keys('id', 'name', 'description');
                    }

                    done();
                })
        })
    }) 

})
