const chai = require('chai');
const { expect } = chai;

const Organism = require('../../app/models/entities/organism.entity');
const organismServiceFactory = require('../../app/services/organism.service');

describe("Organism routes", () => {

    const OrganismMock = {};
    const organismService = organismServiceFactory({ Organism: OrganismMock });

    describe("findAll", () => {
        it("should list two organisms", async () => {
            OrganismMock.count = () => Promise.resolve(2);
            OrganismMock.find = () => ({
                skip: () => ({
                    limit: () => Promise.resolve([{}, {}])
                })
            });

            const pagedOrganisms = await organismService.findAll(0, 10, {});
            
            expect(pagedOrganisms).to.be.an('object');
            expect(pagedOrganisms.total).to.be.equals(2);
            expect(pagedOrganisms.data).to.be.an('array');
            expect(pagedOrganisms.data.length).to.be.equals(pagedOrganisms.total);
        })
    }) 

})
